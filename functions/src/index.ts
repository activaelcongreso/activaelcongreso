import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp(functions.config().firebase);


const cors = require('cors')({
  origin: true,
});

export const contarLlamado = functions.https.onRequest((request, response) => {
	cors(request, response, () => {
		if (request.method === 'POST') {
			const tipoLegis: string = request.body.tipoLegis;
			const id: string = request.body.id;
			const campaign: string = request.body.campaign;
			if ((campaign === "total") || (!campaign) || (!id) || (!tipoLegis)) {
				return response.status(403).send('Forbidden!');
			}
			
			const callRef = admin.database().ref(`/counters/${campaign}/${tipoLegis}/${id}/llamados`);
			console.log("llamando incremento tweets para ", id);
			
			callRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			const totalCampaignRef = admin.database().ref(`/counters/${campaign}/total/llamados`);
			totalCampaignRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			const totalRef = admin.database().ref(`/counters/total/llamados`);
			totalRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});
			
			return response.status(200).send('OK');
		} else {
			return response.status(403).send('Forbidden!');
		}
	});
});

export const contarTweet = functions.https.onRequest((request, response) => {
	cors(request, response, () => {
		if (request.method === 'POST') {
			const tipoLegis: string = request.body.tipoLegis;
			const id: string = request.body.id;
			const campaign: string = request.body.campaign;
			if ((campaign === "total") || (!campaign) || (!id) || (!tipoLegis)) {
				return response.status(403).send('Forbidden!');
			}
			
			const callRef = admin.database().ref(`/counters/${campaign}/${tipoLegis}/${id}/tweets`);
			console.log("llamando incremento tweets para ", id);
			
			callRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			const totalCampaignRef = admin.database().ref(`/counters/${campaign}/total/tweets`);
			totalCampaignRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			const totalRef = admin.database().ref(`/counters/total/tweets`);
			totalRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			return response.status(200).send('OK');
		} else {
			return response.status(403).send('Forbidden!');
		}
	});
});

export const contarFacebook = functions.https.onRequest((request, response) => {
	cors(request, response, () => {
		if (request.method === 'POST') {
			const tipoLegis: string = request.body.tipoLegis;
			const id: string = request.body.id;
			const campaign: string = request.body.campaign;
			if ((campaign === "total") || (!campaign) || (!id) || (!tipoLegis)) {
				return response.status(403).send('Forbidden!');
			}
			
			const callRef = admin.database().ref(`/counters/${campaign}/${tipoLegis}/${id}/fbs`);
			console.log("llamando incremento tweets para ", id);
			
			callRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			const totalCampaignRef = admin.database().ref(`/counters/${campaign}/total/fbs`);
			totalCampaignRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			const totalRef = admin.database().ref(`/counters/total/fbs`);
			totalRef.transaction( current_value => {
				return (current_value || 0) + 1;
			});

			return response.status(200).send('OK');
		} else {
			return response.status(403).send('Forbidden!');
		}
	});
});

