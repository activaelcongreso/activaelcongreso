export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC7jtsd3ZxF8dx_ThXU5ehjfbBOnSliPpI",
    authDomain: "activaelcongreso-8248f.firebaseapp.com",
    databaseURL: "https://activaelcongreso-8248f.firebaseio.com",
    projectId: "activaelcongreso-8248f",
    storageBucket: "activaelcongreso-8248f.appspot.com",
    messagingSenderId: "248427351213",
    appId: "1:248427351213:web:426d95e57791f060"
  },
  contarTweetUrl: "https://us-central1-noalareformalaboral-a655b.cloudfunctions.net/contarTweet",
  contarLlamadoUrl: "https://us-central1-noalareformalaboral-a655b.cloudfunctions.net/contarLlamado",
  contarFacebookUrl: "https://us-central1-noalareformalaboral-a655b.cloudfunctions.net/contarFacebook",
  sentry_dsn: "https://cf8838068bd7425e8d04778aed90ddbe@sentry.io/1229279"
};
