// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC7jtsd3ZxF8dx_ThXU5ehjfbBOnSliPpI",
    authDomain: "activaelcongreso-8248f.firebaseapp.com",
    databaseURL: "https://activaelcongreso-8248f.firebaseio.com",
    projectId: "activaelcongreso-8248f",
    storageBucket: "activaelcongreso-8248f.appspot.com",
    messagingSenderId: "248427351213",
    appId: "1:248427351213:web:426d95e57791f060"
  },
  contarTweetUrl: "https://us-central1-activaelcongreso-dev.cloudfunctions.net/contarTweet",
  contarLlamadoUrl: "https://us-central1-activaelcongreso-dev.cloudfunctions.net/contarLlamado",
  contarFacebookUrl: "https://us-central1-activaelcongreso-dev.cloudfunctions.net/contarFacebook",
  sentry_dsn: "invalid"
};

/*firebase: {
  apiKey: 'AIzaSyDzVfjDQazHRPzJoYmhKRYpHktYEyhbuvg',
  authDomain: 'activaelcongreso-dev.firebaseapp.com',
  databaseURL: 'https://activaelcongreso-dev.firebaseio.com',
  projectId: 'activaelcongreso-dev',
  storageBucket: 'activaelcongreso-dev.appspot.com',
  messagingSenderId: '40510277873'
} */