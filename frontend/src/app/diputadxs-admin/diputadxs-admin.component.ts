import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-diputadxs-admin',
  templateUrl: './diputadxs-admin.component.html',
  styleUrls: ['./diputadxs-admin.component.css']
})
export class DiputadxsAdminComponent implements OnInit {
  public items: Observable<any[]>;
  constructor(private af: AngularFireDatabase) {
    this.items = this.af.list("/diputadxs").valueChanges();
  }

  ngOnInit() {
  }

}
