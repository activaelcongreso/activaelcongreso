import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiputadxsAdminComponent } from './diputadxs-admin.component';

describe('DiputadxsAdminComponent', () => {
  let component: DiputadxsAdminComponent;
  let fixture: ComponentFixture<DiputadxsAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiputadxsAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiputadxsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
