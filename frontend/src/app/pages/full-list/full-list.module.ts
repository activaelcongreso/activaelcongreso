import { NgModule } from "@angular/core";
import { FullListComponent } from './full-list.component';
import { RouterModule, Routes } from '@angular/router';
import { RoutesHelper } from '../../helpers/routes.helper';
import { ComponentsModule } from '../components/components.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MainComponent } from '../main/main.component';
import { ProjectViewComponent } from '../project-view/project-view.component';
import { AdminComponent } from '../admin/admin.component';

const AppRoutes: Routes =  [
    { path: '', pathMatch: 'full', component: MainComponent },
    { path: "proyecto/:id", component: ProjectViewComponent },
    { path: "lista/:id", component: FullListComponent },
    { path: 'FA1776FE544C44FAD1CF2BEC71A14464', component: AdminComponent },
    { path: '**', redirectTo: '' }
];


@NgModule({
    declarations: [
        FullListComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(AppRoutes, { enableTracing: false }),
        ComponentsModule,
        FormsModule
    ],
    exports: [
        FullListComponent
    ]
})
export class FullListModule {

}
