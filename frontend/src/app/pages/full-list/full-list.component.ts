import { Component } from "@angular/core";
import { Proyecto } from '../../model/clases/proyectos';
import { ActivatedRoute } from '@angular/router';
import { ProyectoService } from '../../model/servicios/proyecto.service';
import { Diputadx } from '../../model/clases/diputadxs';
import { DiputadxService } from '../../model/servicios/diputadx.service';
import { Constantes } from '../admin/components/lista-legis/constantes';

@Component({
    selector: 'full-list-page',
    templateUrl: './full-list.component.html',
    styleUrls: ['./full-list.component.scss']
})
export class FullListComponent {

    public proyecto: Proyecto = new Proyecto();
    public politics: Diputadx[];
    public showingPolitics: Diputadx[];
    public filPos: string = "Todas";
    public filBloq: string = "Todos";
    public filDis: string = "Todos";
    public bloquesOptions;
    public distritoOptions = Constantes.distritos;

    constructor(private route: ActivatedRoute,
        private proyectoService: ProyectoService,
        private politicService: DiputadxService) {
        this.route.paramMap.subscribe(params => {
            this.proyectoService
                .traerPorId(params['params']['id'])
                .then(p => {
                    this.proyecto = p;
                    this.getPolitics();
                    this.setOptions();
                });
        });
    }

    private getPolitics(): void {
        this.politicService
            .traerPorCargo(this.proyecto.objetivo)
            .then(rta => {
                this.politics = rta;
                this.showingPolitics = this.politics;
            });
    }

    private setOptions() {
        if(this.proyecto.objetivo == 'sen') {
            this.bloquesOptions = Constantes.bloquesSen;
        } else if(this.proyecto.objetivo = 'dip') {
            this.bloquesOptions = Constantes.bloquesDip;
        } else if(this.proyecto.objetivo = 'leg') {
            this.bloquesOptions = Constantes.bloquesLeg;
        }
    }

    public onFilterChange(filtro: string, event: any) {
        if(filtro == 'posicion') {
            this.showingPolitics = this.filterPosition(this.politics, event);
            this.showingPolitics = this.filterBloque(this.showingPolitics, this.filBloq);
            this.showingPolitics = this.filterDistrito(this.showingPolitics, this.filDis);
        } else if(filtro == 'bloque') {
            this.showingPolitics = this.filterBloque(this.politics, event);
            this.showingPolitics = this.filterPosition(this.showingPolitics, this.filPos);
            this.showingPolitics = this.filterDistrito(this.showingPolitics, this.filDis);
        } else if(filtro == 'distrito') {
            this.showingPolitics = this.filterDistrito(this.politics, event);
            this.showingPolitics = this.filterPosition(this.showingPolitics, this.filPos);
            this.showingPolitics = this.filterBloque(this.showingPolitics, this.filBloq);
        }
    }

    private filterPosition(list: Diputadx[], filterPos: string) {
        return list.filter(p => {
            let posicion;
            p.posiciones.forEach(pos => {
                if(pos.proyecto == this.proyecto.id) {
                    posicion = pos.posicion;
                }
            });
            return posicion == filterPos || filterPos == 'Todas';
        });
    }

    private filterBloque(list: Diputadx[], filterBloq: string) {
        return list.filter(p => p.bloque == filterBloq || filterBloq == 'Todos');
    }

    private filterDistrito(list: Diputadx[], filterDist: string) {
        return list.filter(p => p.distrito == filterDist || filterDist == 'Todos');
    }

    public resetFilter(): void {
        this.filPos = 'Todas';
        this.filBloq = 'Todos';
        this.filDis = 'Todos';
        this.showingPolitics = this.politics;
    }

}