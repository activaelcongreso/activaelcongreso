import { NgModule } from "@angular/core";
import { LegSliderComponent } from './components/leg-slider/leg-slider.component';
import { ProjectViewComponent } from './project-view.component';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentsModule } from '../components/components.module';
import { RouterModule, Routes } from '@angular/router';
import { SliderComponent } from './components/slider/slider.component';
import { VidSliderComponent } from './components/vid-slider/vid-slider.component';
import { ArticleComponent } from './components/article/article.component';
import { MainComponent } from '../main/main.component';
import { FullListComponent } from '../full-list/full-list.component';
import { AdminComponent } from '../admin/admin.component';

const AppRoutes: Routes =  [
    { path: '', pathMatch: 'full', component: MainComponent },
    { path: "proyecto/:id", component: ProjectViewComponent },
    { path: "lista/:id", component: FullListComponent },
    { path: 'FA1776FE544C44FAD1CF2BEC71A14464', component: AdminComponent },
    { path: '**', redirectTo: '' }
];

@NgModule({
    declarations: [
        ProjectViewComponent,
        SliderComponent,
        LegSliderComponent,
        VidSliderComponent,
        ArticleComponent
    ],
    imports: [
        ComponentsModule,
        BrowserModule,
        RouterModule.forRoot(AppRoutes, { enableTracing: false })
    ]
})
export class ProjectViewModule {

}