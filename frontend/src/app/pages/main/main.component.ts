import { Component } from "@angular/core";
import { ProyectoService } from '../../model/servicios/proyecto.service';
import { Proyecto } from '../../model/clases/proyectos';
import { ConfigService } from '../../model/servicios/config.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GFormService } from '../../model/servicios/gform.service';

@Component({
    selector: 'main-page',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent {

    public proyList: Array<Proyecto>; 
    public contador: number = 0;
    public formGroup: FormGroup;
    public newsletterMessage: string;

    constructor(public proyService: ProyectoService,
            public configService: ConfigService,
            public fb: FormBuilder, 
            public gformService: GFormService) {
        this.proyService
            .traerTodos()
            .then(rta => this.proyList = rta);
        this.configService
                .getContador()
                .then(c => this.contador = c);
        
        this.formGroup = fb.group({
            email: ['', Validators.compose([Validators.email, Validators.required])],
        });

    }

    public submitNewsletter() {
        this.newsletterMessage = '';
        if(this.formGroup.valid) {
            this.newsletterMessage = 'Suscripción realizada';
            this.gformService
                .submitForm("AKfycbyX7QdT_mmyPc-bD-40UNdzrVnm90XxbzsujvKR1H3VHIqRPg", this.formGroup.controls)
                .then(rta => {
                    this.formGroup.controls['email'].setValue(null);
                })
        } else {
            this.newsletterMessage = 'Ingrese un email válido';
        }
    }


}