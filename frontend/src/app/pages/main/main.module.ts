import { NgModule } from "@angular/core";
import { MainComponent } from "./main.component";
import { ProyectoService } from '../../model/servicios/proyecto.service';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { ConvinceComponent } from '../../components/convince/convince.component';
import { ContactComponent } from '../../components/contact/contact.component';
import { AdminComponent } from '../admin/admin.component';
import { ProjectViewComponent } from '../project-view/project-view.component';
import { ProjectCardComponent } from './components/project-card/project-card.component';
import { ConfigService } from '../../model/servicios/config.service';
import { ComponentsModule } from '../components/components.module';
import { FullListComponent } from '../full-list/full-list.component';
import { GFormService } from '../../model/servicios/gform.service';
import { ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', component: MainComponent },
    { path: "proyecto/:id", component: ProjectViewComponent },
    { path: "lista/:id", component: FullListComponent },
    { path: 'FA1776FE544C44FAD1CF2BEC71A14464', component: AdminComponent },
    { path: '**', redirectTo: '' }
];

@NgModule({
    declarations: [
        MainComponent,
        ProjectCardComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes, { enableTracing: false }),
        ComponentsModule,
        ReactiveFormsModule
    ],
    providers: [
        ProyectoService,
        ConfigService,
        GFormService
    ]
})
export class MainModule {
}