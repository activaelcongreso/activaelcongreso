import { Component, Input, OnInit } from "@angular/core";
import { Proyecto } from '../../../../model/clases/proyectos';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'project-card',
    templateUrl: 'project-card.component.html',
    styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnInit {
    
    @Input() project: Proyecto;
    public bkg: string;
    
    public constructor(public sanitizer: DomSanitizer) {
    }

    public sanitize(style): any {
        return this.sanitizer.bypassSecurityTrustStyle(style);
    }

    public ngOnInit(): void {
        let bkgAux = 'linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url(https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/' + this.project.img + '?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210) no-repeat';
        this.bkg = this.sanitize(bkgAux);
    }

}