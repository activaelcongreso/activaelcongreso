import { NgModule } from "@angular/core";
import { ContadorComponent } from './contador/contador.component';
import { PoliticCardComponent } from './politic-card/politic-card.component';
import { CommonModule } from '@angular/common';
import { ConteoComponent } from './conteo/conteo.component';

@NgModule({
    declarations: [
        ContadorComponent,
        PoliticCardComponent,
        ConteoComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ContadorComponent,
        PoliticCardComponent,
        ConteoComponent
    ]
})
export class ComponentsModule {
}