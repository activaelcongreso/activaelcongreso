import { Component, Input, OnInit, AfterViewChecked } from "@angular/core";
import { Diputadx } from '../../../model/clases/diputadxs';
import { Proyecto } from '../../../model/clases/proyectos';
import { ConfigService } from '../../../model/servicios/config.service';
import { ProyectoService } from '../../../model/servicios/proyecto.service';

@Component({
    selector: 'politic-card',
    templateUrl: './politic-card.component.html',
    styleUrls: ['./politic-card.component.scss']
})
export class PoliticCardComponent implements OnInit {

    @Input() politicx: Diputadx;
    @Input() set proyecto(value: Proyecto) {
        this._proyecto = value;
        if(this._proyecto.nombre) {
            this.politicx.posiciones.forEach(pos => {
                if(pos.proyecto == this._proyecto.id) {
                    this.posicion = pos.posicion;
                }
            });
        }
    };

    public _proyecto: Proyecto;
    public imgUrl: string;
    public posicion: string;
    public randomFbMessage: string;

    public constructor(public configService: ConfigService,
        public proyectoService: ProyectoService) {
    }

    public ngOnInit(): void {
        this.imgUrl = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.politicx.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
        this.generateRandomMesssage();
    }

    public openTwWindow(): void {
        this.aumentarContador();
        let randomTwit = this.getRandomTwit().replace('@', '@' + this.politicx.twitter);
        window.open('https://twitter.com/intent/tweet?text='+encodeURI(randomTwit), "_blank");
    }

    public generateRandomMesssage(): void {
        this.randomFbMessage = this.getRandomTwit().replace('@', this.politicx.nombre);;
    }

    public openFbWindow(): void {
        this.aumentarContador();
        window.open(`https://facebook.com/${this.politicx.facebook}`, "_blank");
    }

    public getRandomTwit(): string {
        let positionTwits = this._proyecto.twits.filter(tw => tw.posicion == this.posicion);
        let length = positionTwits.length;
        if (length > 0) {
            let index = Math.floor(Math.random() * length);
            return positionTwits[index].twit;
        } else {
            return '';
        }
    }

    public aumentarContador(): void {
        this.configService.aumentarContador();
        this.proyectoService.aumentarContador(this._proyecto);
        this.proyectoService
            .traerCont(this._proyecto.id)
            .then(c => {
                this._proyecto.contador = c;
            })
    }

}