import { NgModule } from "@angular/core";
import { AdminComponent } from "./admin.component";
import { LoginComponent } from "./components/login/login.component";
import { FormsModule } from "@angular/forms";
import { SelectComponent } from "./components/select/select.component";
import { HttpService } from "../../model/servicios/http.service";
import { DiputadxService } from "../../model/servicios/diputadx.service";
import { ProyectoService } from '../../model/servicios/proyecto.service';
import { HttpModule } from "@angular/http";
import { ListaLegisComponent } from "./components/lista-legis/lista-legis.component";
import { CardModule } from 'primeng/card';
import { BrowserModule } from "@angular/platform-browser";
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DetailLLComponent } from "./components/lista-legis/components/detail-ll/detail-ll.component";
import { SidebarModule } from 'primeng/sidebar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CalendarModule } from 'primeng/calendar';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { FirebaseStorageHelper } from '../../helpers/firebase-storage.helper';
import { ProjectComponent } from './components/project/project.component';
import { DetailPComponent } from './components/project/components/detail-p/detail-p.component';
import { ItemsListComponent } from './components/project/components/items-list/items-list.component';
import { DialogModule } from 'primeng/dialog';
import { PosicionesComponent } from './components/lista-legis/components/posiciones/posiciones.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { AutoCompleteModule } from 'primeng/autocomplete';

@NgModule({
    declarations: [
        AdminComponent,
        LoginComponent,
        SelectComponent,
        ListaLegisComponent,
        DetailLLComponent,
        ProjectComponent,
        DetailPComponent,
        ItemsListComponent,
        PosicionesComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        CardModule,
        OverlayPanelModule,
        SidebarModule,
        ConfirmDialogModule,
        CalendarModule,
        AngularFireStorageModule,
        DialogModule,
        CKEditorModule,
        AutoCompleteModule
    ],
    providers: [
        HttpService,
        DiputadxService,
        FirebaseStorageHelper,
        ProyectoService
    ]
})
export class AdminModule {
}