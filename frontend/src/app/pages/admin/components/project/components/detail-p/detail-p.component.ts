import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { BehaviorSubject, Subscription } from "rxjs";
import { FirebaseStorageHelper } from '../../../../../../helpers/firebase-storage.helper';
import { Proyecto } from '../../../../../../model/clases/proyectos';
import { ProyTwit } from '../../../../../../model/clases/proyTwit';
import { ProyVid } from '../../../../../../model/clases/proyVid';
import Swal from 'sweetalert2';

@Component({
    selector: 'detail-p',
    templateUrl: 'detail-p.component.html',
    styles: [`
        label {
            font-size: 16px;
            margin-top: 16px;
            margin-bottom: 2px;
        }

        img {
            border-radius: 9px;
            border: solid 1px #ced4da;
            width: 200px;
            margin-top: 10px;
        }

        .form-check-input {
            margin-top: 21px;
        }

    `]
})
export class DetailPComponent implements OnInit, OnDestroy{
        
    @Input('show') $show: BehaviorSubject<boolean>;
    @Input('selectedProy') set selectedProy(p: Proyecto) { this.onProyChange(p) };
    
    public showSub: Subscription;
    public show: boolean;
    public originalProy: Proyecto;
    public editingProy: Proyecto = new Proyecto();
    public fileLink: string;

    public twitsAFavor: Array<ProyTwit>;
    public twitsEnContra: Array<ProyTwit>;
    public twitsNoConfirmado: Array<ProyTwit>;
    public twitsSeAbstiene: Array<ProyTwit>;

    public newPTAFavor = () => new ProyTwit('AFavor');
    public newPTEnContra = () => new ProyTwit('EnContra');
    public newPTNoConfirmado = () => new ProyTwit('NoConfirmado');
    public newPTSeAbstiene = () => new ProyTwit('SeAbstiene');
    public newPoryVid = () => new ProyVid();

    constructor(private fsHelper: FirebaseStorageHelper) {
        this.editingProy.img = 'nada';
        this.fileLink = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.editingProy.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
    }

    ngOnInit(): void {
        this.showSub = this.$show.subscribe( v => this.show = v );
    }

    ngOnDestroy(): void {
        this.showSub.unsubscribe();
    }

    public onProyChange(p: Proyecto): void {
        if(p) {
            this.originalProy = p;
            this.editingProy = new Proyecto();
            this.editingProy.asignProps(p);
            if(!this.editingProy.img) {
                this.editingProy.img = 'nadaProy';
            }
            this.fileLink = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.editingProy.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
            this.twitsAFavor = this.editingProy.twits.filter(tw => tw.posicion == 'AFavor');
            this.twitsEnContra = this.editingProy.twits.filter(tw => tw.posicion == 'EnContra');
            this.twitsNoConfirmado = this.editingProy.twits.filter(tw => tw.posicion == 'NoConfirmado');
            this.twitsSeAbstiene = this.editingProy.twits.filter(tw => tw.posicion == 'SeAbstiene');
        }
    }

    public save(): void {
        Swal.fire({
            title: '¿Querés guardar los cambios?',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                this.assignTwits();
                this.originalProy.asignProps(this.editingProy);
                this.$show.next(false);
            }
        });

    }

    public assignTwits(): void {
        this.editingProy.twits = new Array<ProyTwit>();
        this.twitsAFavor.forEach(t => this.editingProy.twits.push(t));
        this.twitsEnContra.forEach(t => this.editingProy.twits.push(t));
        this.twitsNoConfirmado.forEach(t => this.editingProy.twits.push(t));
        this.twitsSeAbstiene.forEach(t => this.editingProy.twits.push(t));
    }

    public cancel(): void {
        Swal.fire({
            title: '¿Seguro que querés cancelar?',
            text: 'Los cambios que hayas hecho se van a perder',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                this.$show.next(false);
                this.onProyChange(this.originalProy);
            }
        });
    }

    public uploadFile(event: any): void {
        let timeStamp = (Math.floor(Date.now() / 1000)).toString();
        this.fsHelper.upload(event.target.files[0], timeStamp).then(() => {
            this.editingProy.img = timeStamp;
            this.editingProy.fotoExt = event.target.files[0].name.split('.').pop();
            this.fileLink = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.editingProy.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
        });
    }

}