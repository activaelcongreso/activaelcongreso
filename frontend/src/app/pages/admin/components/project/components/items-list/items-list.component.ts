import { Component, Input } from "@angular/core";

@Component({
    selector: 'items-list',
    templateUrl: './items-list.component.html',
    styles: [`
        .card-body {
            padding: 5px;
            background-color: #f5f6f5;
        }

        .btn-toolbar {
            margin-bottom: 6px; 
            float: right;
        }

        .add-option-input {
            width: 100%;
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .pi {
            float:right;
            font-size: 2em;
            margin-top: -3px;
        }
    `]
})
export class ItemsListComponent {

    @Input() getNew: ()=>any;
    @Input() list: any[];
    @Input() stringField: string;

    public newItemString: string;

    public remove(it: any): void {
        let newList = this.list.filter(i => i != it);
        this.clearListAndAdd(newList);
    }

    public moveUp(it: any): void {
        let itemIndex = this.list.indexOf(it) - 1;
        let listTop = this.list.filter(i => this.list.indexOf(i) < itemIndex);
        let listBottom = this.list.filter(i => this.list.indexOf(i) >= itemIndex && i != it);
        let newList = new Array<any>();
        let aux = listTop.concat(it).concat(listBottom);
        aux.forEach(i => {
            let newIt = this.getNew();
            newIt.asignProps(i);
            newList.push(newIt);
        });
        this.clearListAndAdd(newList);   
    }

    public moveDown(it: any): void {
        let itemIndex = this.list.indexOf(it) + 1;
        let listTop = this.list.filter(i => this.list.indexOf(i) <= itemIndex  && i != it);
        let listBottom = this.list.filter(i => this.list.indexOf(i) > itemIndex);
        let newList = new Array<any>();
        let aux = listTop.concat(it).concat(listBottom);
        aux.forEach(i => {
            let newIt = this.getNew();
            newIt.asignProps(i);
            newList.push(newIt);
        });
        this.clearListAndAdd(newList);
    }

    public addOption(): void {
        let newItem = this.getNew();
        let toAddString = this.newItemString;
        if(this.stringField == 'video') {
            toAddString = 'https://www.youtube.com/embed/' + toAddString.split('v=')[1];
        }
        newItem[this.stringField] = toAddString;
        this.list.push(newItem);
        this.newItemString = undefined;
    }

    public clearListAndAdd(newList: any[]): void {
        while (this.list.length > 0) {
            this.list.pop();
        }
        newList.forEach(i => {this.list.push(i)})
    }

}