import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Proyecto } from '../../../../model/clases/proyectos';
import { BehaviorSubject } from 'rxjs';
import { FirebaseStorageHelper } from '../../../../helpers/firebase-storage.helper';
import { ProyectoService } from '../../../../model/servicios/proyecto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'project',
  templateUrl: './project.component.html',
  styles: [`
      .container-fluid {
        background-color: #006b67;
        min-height: 90.7vh;
      }

      .highlight-hover:hover {
        color: #fcde64;
        cursor: pointer;
      }

      .ui-overlaypanel-content {
        margin-top: -7px;
        padding: 0px !important;
      }

      .list-group-item {
        cursor: pointer;
      }

      .list-group-item:hover {
        background-color: #ececec;
      }

      .btn-back {
        margin-right: calc(100% - 131.6px) !important;
      }

  `]
})
export class ProjectComponent implements OnInit {

    @Output() showSelect: EventEmitter<any> = new EventEmitter();

    public listaProy: BehaviorSubject<Proyecto[]>;
    public listaProyOriginal: Proyecto[];
    public showDetail: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public selectedProy: Proyecto;
    public buscando: string;
    public addingProy: Proyecto = new Proyecto();

    constructor(private proyectoService: ProyectoService, 
                private fsHelper: FirebaseStorageHelper) {
      this.listaProy = new BehaviorSubject<Proyecto[]>(new Array<Proyecto>());
      this.traerTodos();
    }

    public ngOnInit(): void {
      this.showDetail.subscribe(f => {
        this.createProyecto();
        let newList = new Array<Proyecto>();
        this.listaProy.value.forEach(f => { newList.push(f); });
        this.listaProy.next(newList);
        if(!f) {
          this.proyectoService.update(this.listaProy.value);
        }

      });
    }

    private traerTodos(): void {
      this.proyectoService
        .traerTodos()
        .then(res => {
          this.listaProy.next(res);
          this.listaProyOriginal = res;
        });
    }

    private createProyecto(): void {
      if(this.addingProy.nombre) {
        this.addingProy.generateId();
        this.proyectoService.create(this.addingProy);
        this.listaProy.value.push(this.addingProy);
        this.selectedProy = new Proyecto();
        this.addingProy = null;
      }
    }

    public buscandoChanges(): void {
      if(this.buscando) {
        this.listaProy.next(
          this.listaProyOriginal.filter( d => { 
            return d.nombre.includes(this.buscando);
          })
        );
      } else {
        this.traerTodos();
      }
    }

    public delete() {
      Swal.fire({
        title: '¿Seguro que querés eliminar este proyecto?',
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'Sí',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.doDelete();
        }
      });
    }

    public doDelete() {
      this.proyectoService
      .delete(this.selectedProy)
      .then(() => {
        if(this.selectedProy.img != 'nadaProy') {
          this.fsHelper.delete(this.selectedProy.img);
        }
        this.listaProyOriginal = this.listaProyOriginal.filter(d => d!= this.selectedProy);
        this.listaProy.next(this.listaProyOriginal.filter(d => d!= this.selectedProy));
        this.selectedProy = this.listaProy.value[0]
      });
    }

    public addProy(): void {
      this.selectedProy = this.addingProy;
      this.showDetail.next(true);
    }

    public openOverlay(op1: any, $event: any, proy: Proyecto): void{
      op1.toggle($event);
      this.selectedProy = proy;
    }

    public doShowDetail(): void {
      this.showDetail.next(true);
    }

    public goToSelect(): void {
      this.showSelect.next();
    }

}