import { Component, Input } from "@angular/core";
import { PosLeg } from '../../../../../../model/clases/posLeg';

@Component({
    selector: 'posiciones',
    templateUrl: './posiciones.component.html'
}) 
export class PosicionesComponent {

    @Input() posiciones: Array<PosLeg>;

}