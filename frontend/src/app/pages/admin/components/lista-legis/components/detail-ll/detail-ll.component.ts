import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { BehaviorSubject, Subscription } from "rxjs";
import { Diputadx } from "../../../../../../model/clases/diputadxs";
import { FirebaseStorageHelper } from '../../../../../../helpers/firebase-storage.helper';
import { Constantes } from '../../constantes';
import Swal from 'sweetalert2'

@Component({
    selector: 'detail-ll',
    templateUrl: 'detail-ll.component.html',
    styles: [`
        label {
            font-size: 16px;
            margin-top: 16px;
            margin-bottom: 2px;
        }

        img {
            border-radius: 9px;
            border: solid 1px #ced4da;
            width: 200px;
            margin-top: 10px;
        }

        .file-button {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            transition: color 
                .15s ease-in-out,background-color 
                .15s ease-in-out,border-color 
                .15s ease-in-out,box-shadow 
                .15s ease-in-out;
        }

        input[type="file"] {
            display: none;
        }
    `]
})
export class DetailLLComponent implements OnInit, OnDestroy{
        
    @Input('show') $show: BehaviorSubject<boolean>;
    @Input('selectedDip') set selectedDip(d: Diputadx) { this.onDipChange(d) };
    
    public showSub: Subscription;
    public show: boolean;
    public originalDip: Diputadx;
    public editingDip: Diputadx = new Diputadx();
    public fileLink: string;
    public showDialog = false;
    public bloquesOptions = Constantes.bloquesDip;
    public distritoOptions = Constantes.distritos;
    public cargoOptions: string[];

    constructor(private fsHelper: FirebaseStorageHelper) {
        this.editingDip.img = 'nada';
        this.fileLink = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.editingDip.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
    }

    ngOnInit(): void {
        this.showSub = this.$show.subscribe( v => this.show = v );
    }

    ngOnDestroy(): void {
        this.showSub.unsubscribe();
    }

    public onDipChange(d: Diputadx): void {
        if(d) {
            this.originalDip = d;
            this.editingDip = new Diputadx();
            this.editingDip.asignProps(d);
            this.onCargoChanges();
            if(!this.editingDip.img) {
                this.editingDip.img = 'nada';
            }
            this.fileLink = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.editingDip.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
        }
    }

    public onCargoChanges(): void {
        if(this.editingDip.cargo == 'Diputadx') {
            this.bloquesOptions = Constantes.bloquesDip;
        } else if(this.editingDip.cargo == 'Senadorx') {
            this.bloquesOptions = Constantes.bloquesSen;
        } else if(this.editingDip.cargo == 'Legisladorx') {
            this.bloquesOptions = Constantes.bloquesLeg;
        } 
    }

    public search(event): void {
        this.cargoOptions = ['Diputadx', 'Senadorx', 'Legisladorx'];
    }

    public save(): void {
        Swal.fire({
            title: '¿Querés guardar los cambios?',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                this.originalDip.asignProps(this.editingDip);
                this.$show.next(false);
            }
        });
    }

    public cancel(): void {
        Swal.fire({
            title: '¿Seguro que querés cancelar?',
            text: 'Los cambios que hayas hecho se van a perder',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value) {
                this.$show.next(false);
                this.onDipChange(this.originalDip);
            }
        });
    }

    public uploadFile(event: any) {
        let timeStamp = (Math.floor(Date.now() / 1000)).toString();
        this.fsHelper.upload(event.target.files[0], timeStamp).then(() => {
            this.editingDip.img = timeStamp;
            this.editingDip.imgExt = event.target.files[0].name.split('.').pop();
            this.fileLink = "https://firebasestorage.googleapis.com/v0/b/activaelcongreso-8248f.appspot.com/o/" + this.editingDip.img + "?alt=media&token=163d9491-bb4c-4c87-93bc-b39c46121210";
        });
    }

}