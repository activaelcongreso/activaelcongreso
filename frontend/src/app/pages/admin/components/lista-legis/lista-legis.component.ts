import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { DiputadxService } from '../../../../model/servicios/diputadx.service';
import { Diputadx } from '../../../../model/clases/diputadxs';
import { BehaviorSubject } from 'rxjs';
import { FirebaseStorageHelper } from '../../../../helpers/firebase-storage.helper';
import Swal from 'sweetalert2';

@Component({
  selector: 'lista-legis',
  templateUrl: './lista-legis.component.html',
  styles: [`
      .container-fluid {
        background-color: #006b67;
        min-height: 90.7vh;
      }

      .highlight-hover:hover {
        color: #fcde64;
        cursor: pointer;
      }

      .ui-overlaypanel-content {
        margin-top: -7px;
        padding: 0px !important;
      }

      .list-group-item {
        cursor: pointer;
      }

      .list-group-item:hover {
        background-color: #ececec;
      }

      .btn-back {
        margin-right: calc(100% - 212px) !important;
      }

  `]
})
export class ListaLegisComponent implements OnInit {

    @Output() showSelect: EventEmitter<any> = new EventEmitter();

    public listaDip: BehaviorSubject<Diputadx[]>;
    public listaDipOriginal: Diputadx[];
    public showDetail: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public selectedDip: Diputadx;
    public buscando: string;
    public addingDip: Diputadx = new Diputadx();

    constructor(private diputadxsService: DiputadxService, 
                private fsHelper: FirebaseStorageHelper) {
      this.listaDip = new BehaviorSubject<Diputadx[]>(new Array<Diputadx>());
      this.traerTodos();
    }

    public ngOnInit(): void {
      this.showDetail.subscribe(f => {
        this.createDiputadx();
        let newList = new Array<Diputadx>();
        this.listaDip.value.forEach(f => {
            newList.push(f);
        });
        this.listaDip.next(newList);
        if(!f) {
          this.diputadxsService.update(this.listaDip.value);
        }

      });
    }

    private createDiputadx(): void {
      if(this.addingDip.nombre && this.addingDip.apellido) {
        this.diputadxsService.create(this.addingDip);
        this.listaDip.value.push(this.addingDip);
        this.selectedDip = new Diputadx();
        this.addingDip = null;
        this.traerTodos();
      }
    }

    private traerTodos(): void {
      this.diputadxsService
        .traerTodos()
        .then(res => {
          this.listaDip.next(res);
          this.listaDipOriginal = res;
        });
    }

    public buscandoChanges(): void {
      if(this.buscando) {
        this.listaDip.next(
          this.listaDipOriginal.filter( d => { 
            return d.nombre.includes(this.buscando) || d.apellido.includes(this.buscando);
          })
        );
      } else {
        this.traerTodos();
      }
    }

    public filter(cargo: string) {
      this.buscando = null;
      if(cargo != 'Ninguno') {
        this.listaDip.next(
          this.listaDipOriginal.filter( d => d.cargo == cargo)
        );
      } else {
        this.listaDip.next(this.listaDipOriginal);
      }
    }

    public delete() {
      Swal.fire({
          title: '¿Seguro que querés eliminar a esta persona?',
          type: 'info',
          showCancelButton: true,
          confirmButtonText: 'Sí',
          cancelButtonText: 'No'
      }).then((result) => {
          if (result.value) {
            this.doDelete();
          }
      });
    }

    public doDelete() {
      this.diputadxsService
      .delete(this.selectedDip)
      .then(() => {
        if(this.selectedDip.img != 'nada') {
          this.fsHelper.delete(this.selectedDip.img);
        }
        this.listaDipOriginal = this.listaDipOriginal.filter(d => d!= this.selectedDip);
        this.listaDip.next(this.listaDipOriginal.filter(d => d!= this.selectedDip));
        this.selectedDip = this.listaDip.value[0]
      });
    }

    public addDip(): void {
      this.selectedDip = this.addingDip;
      this.showDetail.next(true);
    }

    public openOverlay(op1: any, $event: any, dip: Diputadx): void{
      op1.toggle($event);
      this.selectedDip = dip;
    }

    public doShowDetail(): void {
      this.showDetail.next(true);
    }

    public goToSelect(): void {
      this.showSelect.next();
    }

}