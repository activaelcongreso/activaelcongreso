import { Component, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'select-comp',
    templateUrl: './select.component.html',
    styles: [`
      .container-fluid {
        background-color: #006b67;
        min-height: 90.7vh;
      }

      .card {
        margin-top: 50px;
      }
    `]
})
export class SelectComponent {

    @Output() seleccted: EventEmitter<any> = new EventEmitter();

    constructor(){
    }

}