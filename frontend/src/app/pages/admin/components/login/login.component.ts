import { Component, Output, EventEmitter } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styles: [`
      .container-fluid {
        background-color: #006b67;
        min-height: 90.7vh;
      }

      .card {
        margin-top: 50px;
      }
  `]
})
export class LoginComponent {

  public usr: string;
  public pass: string;
  @Output() logged: EventEmitter<any> = new EventEmitter();

  constructor(public  afAuth:  AngularFireAuth) {
  }

  public doLogin(): void {
    try {
      this.afAuth.auth
        .signInWithEmailAndPassword(this.usr, this.pass)
        .then(res => {
          this.logged.emit();
        }).catch(res => {
          alert('Información incorrecta');    
        });
    } catch(e) {
      alert('Información incorrecta');
    }
  }

}
