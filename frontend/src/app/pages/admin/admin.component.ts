import { Component } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'admin-page',
  templateUrl: './admin.component.html'
})
export class AdminComponent {

  public sectionShowing: string = 'login';

  constructor(public  afAuth:  AngularFireAuth) {
  }

  public showSelect(): void {
    this.changeSection('select');
  }

  public onSelected(event: any): void{
    this.changeSection(event);
  }

  public changeSection(section: string) {
    this.sectionShowing = section;
  }

}
