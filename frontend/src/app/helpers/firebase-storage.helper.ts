import { AngularFireStorage } from 'angularfire2/storage';
import { Injectable } from '@angular/core';

@Injectable()
export class FirebaseStorageHelper {

    constructor(private afStorage: AngularFireStorage) {
    }

    public upload(file: any, name: string): Promise<any> {
        return this.afStorage
            .ref(name)
            .put(file)
            .catch(err => alert('upload error'));
    }

    public delete(name): void {
        try {
            this.afStorage
            .ref(name)
            .delete();
        } catch(e) {
            alert('file delete error');
        }
    }

}