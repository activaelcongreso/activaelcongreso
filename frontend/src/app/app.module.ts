//REVISAR CUANDO TERMINE
import * as Raven from 'raven-js';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { MainComponent2 } from './components/main/main.component';
import { AboutReformComponent } from './components/about-reform/about-reform.component';
import { PropuestasComponent } from './components/propuestas/propuestas.component';
import { LegisladoresComponent } from './components/legisladores/legisladores.component';
import { HttpClientModule } from '@angular/common/http';
import { ContactComponent } from './components/contact/contact.component';
import { HorizontalScrollerComponent } from './components/horizontal-scroller/horizontal-scroller.component';
import { UiSpinnerComponent } from './components/ui-spinner/ui-spinner.component';
import { ReadMoreComponent } from './components/read-more/read-more.component';
import { CounterComponent } from './components/counter/counter.component';
import { ConvinceComponent } from './components/convince/convince.component';
import { EventsComponent } from './components/events/events.component';
import { AbortionBillComponent } from './components/abortion-bill/abortion-bill.component';
import { AboutComponent } from './components/about/about.component';
import { MessagesCounterComponent } from './components/messages-counter/messages-counter.component';
import { CoreModule } from './core/core.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { DiputadxsAdminComponent } from './diputadxs-admin/diputadxs-admin.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { TweetsService } from './services/tweets.service';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';

//Nuevas paginas
import { MainComponent } from './pages/main/main.component';
import { MainModule } from './pages/main/main.module';
import { AdminComponent } from './pages/admin/admin.component';
import { AdminModule } from './pages/admin/admin.module';
import { HttpModule } from '@angular/http';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { ProjectViewComponent } from './pages/project-view/project-view.component';
import { HeaderAECComponent } from './components/header-aec/header-aec.component';
import { ProjectViewModule } from './pages/project-view/project-view.module';
import { ComponentsModule } from './pages/components/components.module';
import { FullListComponent } from './pages/full-list/full-list.component';
import { FullListModule } from './pages/full-list/full-list.module';
import { RoutesHelper } from './helpers/routes.helper';


if (environment.sentry_dsn != 'invalid') {
  Raven
  .config(environment.sentry_dsn, {
    release: '20180808-1536',
    shouldSendCallback: function(data) {
      // only send 10% of errors
      const sampleRate = 10;
      return (Math.random() * 100 <= sampleRate);
    }
  }).install();
}
export class RavenErrorHandler implements ErrorHandler {
  handleError(err:any) : void {
    if(environment.sentry_dsn != 'invalid') {
      Raven.captureException(err);
    } else {
      throw err;
    }
  }
}

const AppRoutes: Routes =  [
  { path: '', pathMatch: 'full', component: MainComponent },
  { path: "proyecto/:id", component: ProjectViewComponent },
  { path: "lista/:id", component: FullListComponent },
  { path: 'FA1776FE544C44FAD1CF2BEC71A14464', component: AdminComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent2,
    AboutReformComponent,
    PropuestasComponent,
    LegisladoresComponent,
    ContactComponent,
    HorizontalScrollerComponent,
    UiSpinnerComponent,
    ReadMoreComponent,
    CounterComponent,
    ConvinceComponent,
    EventsComponent,
    AbortionBillComponent,
    AboutComponent,
    MessagesCounterComponent,
    UserProfileComponent,
    DiputadxsAdminComponent,
    HeaderAECComponent
  ],
  imports: [
    ComponentsModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    CoreModule,
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes, { enableTracing: false }),
    HttpClientModule,
    ReactiveFormsModule,
    Ng2GoogleChartsModule,
    MainModule,
    AdminModule,
    FormsModule,
    HttpModule,
    AngularFireStorageModule,
    ProjectViewModule,
    FullListModule
  ],
  providers: [
    { provide: ErrorHandler, useClass: RavenErrorHandler },
    TweetsService,
    {provide: LocationStrategy, useClass: PathLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
