export class PosLeg {

    public id: number;
    public legisladorx: number;
    public proyecto: string;
    public posicion: string;

    public asignProps(pl2: PosLeg) {
        PosLeg.asignProps(this, pl2);
    }

    public static asignProps(pl1: PosLeg, pl2: PosLeg) {
        pl1.id = pl2.id;
        pl1.legisladorx = pl2.legisladorx;
        pl1.proyecto = pl2.proyecto;
        pl1.posicion = pl2.posicion;
    }

    public static getNew() {
        return new PosLeg();
    }

}