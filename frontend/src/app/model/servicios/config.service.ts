import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable()
export class ConfigService {

    constructor(public httpService: HttpService) {
    }

    public getContador(): Promise<any> {
        return this.httpService
            .get('getCont')
            .then(rta => rta._body);
    }

    public aumentarContador(): void {
        this.getContador()
            .then(rta => {
                let newNum = parseInt(rta) + 1;
                this.httpService.post('aumentarCont', newNum);
            });
    }

}