import { Injectable } from "@angular/core";
import { AbstractControl } from "@angular/forms";
import { Http } from '@angular/http';
declare const fbq: any; 

@Injectable()
export class GFormService {

    private appScriptUrl: string = 'https://script.google.com/macros/s/';

    constructor(public http: Http) {
    }

    public submitForm(appId: string, controls: {[key: string]: AbstractControl}): Promise<any> {
        let params = ""; 
        Object.keys(controls).forEach( key => {
            params += "&";
            params += key + "=" + controls[key].value;
        });
        let url = this.appScriptUrl + appId + '/exec?' + params;
        return this.http
            .get(url)
            .toPromise()
            .then(rta => {
                console.log(rta);
            });
    }

}