import { HttpService } from "./http.service";
import { Injectable, ɵConsole } from "@angular/core";
import { Diputadx } from "../clases/diputadxs";

@Injectable()
export class DiputadxService {

    constructor(public httpService: HttpService) {
    }

    public traerTodos(): Promise<Diputadx[]> {
       return this.httpService
            .get('traerDip')
            .then(rta => {
                let primitiveObject = rta.json() as Array<Diputadx>;
                let listaDip = new Array<Diputadx>();
                primitiveObject.forEach(po => {
                    let newDip = new Diputadx();
                    newDip.asignProps(po);
                    listaDip.push(newDip);
                });
                return listaDip;
            });
    }

    public traerPorCargo(cargo: string): Promise<Diputadx[]> {
        return this
            .traerTodos()
            .then(rta => {
                let newList  = new Array<Diputadx>();
                rta.forEach(p => {
                    if(p.cargo == 'Legisladorx' && cargo == 'leg') {
                        newList.push(p);
                    } else if(p.cargo == 'Diputadx' && cargo == 'dip') {
                        newList.push(p);
                    } else if (p.cargo == 'Senadorx' && cargo == 'sen') {
                        newList.push(p);
                    }else if(p.cargo == cargo) {
                        newList.push(p);
                    } 
                });
                return newList;
            })
    }

    public update(listaDip: Array<Diputadx>): void {
        if(listaDip.length > 0) {
            this.httpService
                .post('updateDip', listaDip)
                .catch(rta => alert('upload error'));
        }
    }

    public create(dip: Diputadx): void {
        this.httpService
            .post('createDip', dip)
            .then(rta => console.log(rta))
            .catch(rta => alert('create error'));
    }

    public delete(dip: Diputadx): Promise<any> {
        return this.httpService
            .post('deleteDip', dip)
            .then(rta => rta)
            .catch(rta => alert('delete error'));
    }

}