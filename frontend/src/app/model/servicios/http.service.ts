import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Diputadx } from "../clases/diputadxs";

@Injectable()
export class HttpService {

    private baseUrl = 'http://productoresaconsumidores.com/backend/index.php/';
    //private baseUrl = 'http://lvh.me/activaelcongreso/backend/index.php/';

    constructor(private http: Http) {
    }

    public get(url: string): Promise<any> {
        return this.http
            .get(this.baseUrl + url)
            .toPromise()
            .then(rta => rta)
            .catch(err => err);
    }

    public post(url: string, data: any): Promise<any> {
        return this.http
            .post(this.baseUrl + url, JSON.stringify(data))
            .toPromise()
            .then(resp => resp)
            .catch(err => err);
    }

}