import { HttpService } from "./http.service";
import { Injectable } from "@angular/core";
import { Proyecto } from '../clases/proyectos';

@Injectable()
export class ProyectoService {

    constructor(public httpService: HttpService) {
    }

    public traerTodos(): Promise<Proyecto[]> {
       return this.httpService
            .get('traerProy')
            .then(rta => {
                let primitiveObject = rta.json() as Array<Proyecto>;
                let listaProy = new Array<Proyecto>();
                primitiveObject.forEach(po => {
                    let newProy = new Proyecto();
                    newProy.asignProps(po);
                    listaProy.push(newProy);
                });
                return listaProy;
            });
    }

    public traerPorId(id: string): Promise<Proyecto> {
        return this.traerTodos()
            .then(rta => {
                let proy: Proyecto;
                rta.forEach(p => p.id == id ? proy = p : null);
                return proy;
            })
    }

    public update(listaProy: Array<Proyecto>): void {
        if(listaProy.length > 0) {
            this.httpService
                .post('updateProy', listaProy)
                //.then(rta => console.log(rta)) //debug only
                .catch(rta => alert('update error'));
        }
    }

    public create(proy: Proyecto): void {
        this.httpService
            .post('createProy', proy)
            .then(rta => console.log(rta)) //debug only
            .catch(rta => alert('create error'));
    }

    public delete(dip: Proyecto): Promise<any> {
        return this.httpService
            .post('deleteProy', dip)
            .then(rta => rta)
            .catch(rta => alert('delete error'));
    }

    public traerCont(id: string): Promise<any> {
        return this.traerTodos()
            .then(listaProy => {
                let currentCont: number;
                listaProy.forEach(p => (p.id == id) ? currentCont = p.contador : null);
                return currentCont;
            });
    }

    public aumentarContador(proy: Proyecto): void {
        this.traerCont(proy.id)
            .then(cont => {
                let newNumber:number = parseInt(cont) + 1;
                let data  = {
                    id: proy.id,
                    newNumber: newNumber
                }
                this.httpService.post('aumentarContProy', data);
            });
    }

}