import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import * as _ from "lodash";


@Component({
  selector: 'counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  @Input() set diputadxs(lista: any[]){
    this._items = lista;
    this.posiciones = _.countBy(this._items, 'posicion');
    this.votosFaltantes = 129-this.posiciones["A Favor"];
    //console.log(this.posiciones);
    // "En Contra"
    // "A Favor"
    // "No confirmado"
  };

  @Input() resultadoFinal: boolean = false;
  @Input() showVideo: boolean = true;

  public _items: any[];

  public isXs: boolean;

  public posiciones: any;
  public votosFaltantes: number;

  text:any = {
    Year: 'Año',
    Month: 'Mes',
    Weeks: "Semanas",
    Days: "DÍAS",
    Hours: "HORAS",
    Minutes: "MINUTOS",
    Seconds: "SEGUNDOS",
    MilliSeconds: "Milisegundos"
  };

  public ngOnInit(): void {
    this.setIsXs();
    $(window).resize(this.setIsXs.bind(this));
  }

  public setIsXs(): void {
    this.isXs = $(window).width() < 576;
  }

}
