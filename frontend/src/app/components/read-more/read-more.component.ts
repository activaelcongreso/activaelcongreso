import { Component, Input, OnInit, AfterViewInit, AfterContentInit, AfterContentChecked } from "@angular/core";

@Component({
  selector: 'read-more',
  templateUrl: './read-more.component.html',
  styleUrls: ['./read-more.component.scss']
})
export class ReadMoreComponent implements OnInit{

  @Input() fullText: string[];
  public firstParagraph: string;
  public showButton: boolean = false;
  public readMore: boolean = false;
  public buttonText: string = "MÁS";

  public ngOnInit(): void {
    if(this.fullText && (this.fullText[0].length > 197 || this.fullText.length > 1) ) {
      this.buttonClick();
      this.showButton = true;
      this.firstParagraph = this.fullText[0];
    }
  }

  public buttonClick(): void {
    if(this.readMore) {
      this.buttonText = "MENOS";
      this.readMore = false;
    } else {
      this.buttonText = "MÁS";
      this.readMore = true;
    }
  }

  

}
