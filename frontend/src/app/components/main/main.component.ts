import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as _ from "lodash";
import { AngularFireDatabase } from 'angularfire2/database';
import { Legislator, LegislatorTypeEnum } from '../../interfaces/legislator';
import { TweetsService } from '../../services/tweets.service';

interface Counters {
  tweets: number,
  llamados: number,
  fbs: number
}

interface Config {
  resultadoFinal: boolean,
  showVideo: boolean
}

@Component({
  selector: 'main-page2',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent2 {

  public descriptionText: string[];
  diputadxs: Observable<Legislator[]>;
  senadorxs: Observable<Legislator[]>;
  messages: number = 0;
  resultadoFinal: boolean = false;
  showVideo: boolean = true;

  constructor(public af: AngularFireDatabase, public _tweetsService: TweetsService) {
    this.descriptionText = [("Activá el Congreso es otra forma de expresarnos para que nuestras demandas lleguen más claras y más fuertes a quienes toman decisiones. Activá el Congreso facilita la comunicación directa, sin intermediarxs. Es el momento de hacernos oír, aprovechando todos los medios que tenemos a nuestro alcance.")];
    this.descriptionText.push("El primer eje que proponemos para activar es el Aborto Legal, Seguro y Gratuito. Si se discutiera en el recinto hoy, esto se jugaría voto a voto. Si estás cansadx de que no te escuchen: activá, twitteá o llamá a lxs diputadxs y haceles saber lo que pensás. Hacete protagonista de esta discusión.");
    this.descriptionText.push("Activá el congreso, hagamos historia.");

    this.senadorxs = this.af.list<Legislator>("/senadorxs").snapshotChanges().pipe(
      map(changes => 
        changes.map(c => {
          return new Legislator(
            this._tweetsService,
            c.payload.val().name,
            c.payload.val().lastname,
            c.payload.val().bloque,
            c.payload.val().posicion,
            c.payload.val().twitter,
            c.payload.val().despacho,
            c.payload.val().interno,
            c.payload.val().genero,
            c.payload.val().orden,
            c.payload.val().distrito,
            c.payload.key,
            LegislatorTypeEnum.Senadorx,
            c.payload.val().facebook,
            c.payload.val().disabled,
            c.payload.val().img);
        })
        )
      );
    this.af.object<Counters>('/counters/total').valueChanges().subscribe(msgs => {
      this.messages = msgs.tweets + msgs.llamados + msgs.fbs;
    });
    this.af.object<Config>('/config').valueChanges().subscribe( config => {
      this.resultadoFinal = config.resultadoFinal;
      this.showVideo = config.showVideo;
    });

  }
}
