import { Component, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl } from '@angular/forms';
//import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import * as _ from "lodash";
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../../environments/environment';
import { Legislator, LegislatorTypeEnum } from '../../interfaces/legislator';
import { TweetsService } from '../../services/tweets.service';



@Component({
  selector: 'convince-page',
  templateUrl: './convince.component.html',
  styleUrls: ['./convince.component.scss']
})
export class ConvinceComponent {

  @ViewChild('cchart') cchart;

  tipoLegis: string = 'diputadxs';
  public items: Observable<Legislator[]>;
  public filteredItems: Legislator[];
  public currentDiputadx: Legislator;
  private ordenesRandom: number[];

  form: FormGroup;
  currentDistrict: FormControl;
  currentPosition: FormControl;
  currentParty: FormControl;

  showPositionsChart: boolean = true;
  loading: boolean = true;

  districts = [
    "(todos)",
    "Buenos Aires",
    "Catamarca",
    "Chaco",
    "Chubut",
    "Ciudad Autónoma de Buenos Aires",
    "Córdoba",
    "Corrientes",
    "Entre Ríos",
    "Formosa",
    "Jujuy",
    "La Pampa",
    "La Rioja",
    "Mendoza",
    "Misiones",
    "Neuquén",
    "Río Negro",
    "Salta",
    "San Juan",
    "San Luis",
    "Santa Cruz",
    "Santa Fe",
    "Santiago del Estero",
    "Tierra del Fuego",
    "Tucumán"
  ];

  positions = [
    "(cualquiera)",
    "A Favor",
    "En Contra",
    "No confirmado",
    "Se Abstiene"
  ];

  parties: Array<string>;

  pieChartData =  {
    chartType: 'PieChart',
    dataTable: [
      ['Posición', 'Votos'],
      ['A Favor',   0],
      ['En Contra',  0],
      ['No confirmado', 0],
      ['Se Abstiene', 0]
    ],
    options: {
      'title': 'Votos (con los filtros aplicados)',
      'pieHole': 0.4,
      'colors': ['#00CC6E', '#DB1D19', '#DDA917', 'gray'],
      'backgroundColor': '#00A99D',
      'tooltip': {'text': 'value'},
      'pieSliceText': 'none',
      'titleTextStyle': { 'color': '#FFFFFF', 'fontName': 'Brandon-Bold'},
      'legend': { 'textStyle': {'color': '#FFFFFF', 'fontName': 'Brandon-Medium'} },
    },
  };

  constructor(
      private http: HttpClient, 
      public af: AngularFireDatabase, 
      protected _router: Router, 
      protected _route: ActivatedRoute,
      private _tweetsService: TweetsService) {
    
    this._route.params.subscribe(params => {
      if (params['tipoLegis']) {
        if ((params['tipoLegis'] == 'diputadxs') || (params['tipoLegis'] == 'senadorxs')) {
          this.tipoLegis = params['tipoLegis'];
        }
      }
      let type: LegislatorTypeEnum = this.tipoLegis == 'diputadxs' ? LegislatorTypeEnum.Diputadx : LegislatorTypeEnum.Senadorx;

      this.ordenesRandom = _.shuffle(_.range(0, 257));
      
      this.items = this.af.list<Legislator>(`/${this.tipoLegis}`).snapshotChanges().pipe(
        map(changes => 
          changes.map(c => {
            return new Legislator(
              this._tweetsService,
              c.payload.val().name,
              c.payload.val().lastname,
              c.payload.val().bloque,
              c.payload.val().posicion,
              c.payload.val().twitter,
              c.payload.val().despacho,
              c.payload.val().interno,
              c.payload.val().genero,
              c.payload.val().orden,
              c.payload.val().distrito,
              c.payload.key,
              type,
              c.payload.val().facebook,
              c.payload.val().disabled,
              c.payload.val().img);
          })
          )
        ).map( lista => {
            lista = _.map(lista, (dipu, index) => {
             (dipu as any).orden = this.ordenesRandom[index];
             return dipu;
            });
            lista = _.sortBy(lista, 'orden');

            this.parties = _.concat(["(todos)"], _.uniq(_.map(lista, 'bloque')).sort() );
            this.loading = false;
            return lista;
          }
        );

    this.currentDistrict = new FormControl();
    this.currentPosition = new FormControl();
    this.currentParty = new FormControl();
    this.form = new FormGroup({
      currentDistrict: this.currentDistrict,
      currentPosition: this.currentPosition,
      currentParty: this.currentParty
    });

    this.currentParty.setValue("(todos)");
    this.currentDistrict.setValue("(todos)");
    this.currentPosition.setValue("(cualquiera)");

    let district$ = this.currentDistrict.valueChanges.pipe(startWith("(todos)"));
    let position$ = this.currentPosition.valueChanges.pipe(startWith("(cualquiera)"));
    let party$ = this.currentParty.valueChanges.pipe(startWith("(todos)"));
    combineLatest(this.items, district$, position$, party$).subscribe( ([dipulist, district, position, party]) => {
      this.loading = true;
      let res = dipulist;
      if (district != "(todos)") {
        res = _.filter(res, ['distrito', district]);
        this.parties = _.uniq(_.concat(["(todos)"], _.uniq(_.concat(party, _.map(res, 'bloque'))).sort() ));
      } else {
        this.parties = _.concat(["(todos)"], _.uniq(_.map(dipulist, 'bloque')).sort() );
      }
      if (position != "(cualquiera)") {
        this.showPositionsChart = false;
        res = _.filter(res, ['posicion', position]);
      } else {
        this.showPositionsChart = true;
      }
      if (party != "(todos)") {
        res = _.filter(res, ['bloque', party]);
        let dipulist_thisparty = _.filter(dipulist, ['bloque', party]);
        this.districts = _.concat(["(todos)"], _.uniq(_.map(dipulist_thisparty, 'distrito')).sort() );
      } else {
        this.districts = _.concat(["(todos)"], _.uniq(_.map(dipulist, 'distrito')).sort() );
      }
      


      this.filteredItems = res;
      let votosPorPosicion = _.countBy(res, 'posicion');


      // forces a reference update (otherwise angular won't detect the change
      this.pieChartData = Object.create(this.pieChartData);
      if (votosPorPosicion['A Favor']) {
        this.pieChartData.dataTable[1][1] = votosPorPosicion['A Favor'];  
      } else {
        this.pieChartData.dataTable[1][1] = 0;
      }
      if (votosPorPosicion['En Contra']) {
        this.pieChartData.dataTable[2][1] = votosPorPosicion['En Contra'];  
      } else {
        this.pieChartData.dataTable[2][1] = 0;  
      }
      if (votosPorPosicion['No confirmado']) {
        this.pieChartData.dataTable[3][1] = votosPorPosicion['No confirmado'];  
      } else {
        this.pieChartData.dataTable[3][1] = 0;  
      }
      if (votosPorPosicion['Se Abstiene']) {
        this.pieChartData.dataTable[4][1] = votosPorPosicion['Se Abstiene'];  
      } else {
        this.pieChartData.dataTable[4][1] = 0;  
      }

      this.loading = false;
    });
    
      

    });

  }


  public setCurrentDiputadx(dipu: Legislator): void {
    this.currentDiputadx = dipu;
  }


  public twittearAborto(dipu: Legislator) {
    const url = environment.contarTweetUrl;
    this.http.post(url, {id: dipu.key, tipoLegis:dipu.dbpath, campaign:'aborto'}, {responseType: 'text'}).subscribe(res => console.log(res));
    let tweet = dipu.abortionTweet();
    window.open(`https://twitter.com/intent/tweet?text=${tweet}`, "_blank");
  }

  public contarLlamado(dipu: Legislator) {
    const url = environment.contarLlamadoUrl;
    this.http.post(url, {id: dipu.key, tipoLegis:dipu.dbpath, campaign:'aborto'}, {responseType: 'text'}).subscribe(res => console.log(res));
  }

  public facebookAborto(dipu: Legislator) {
    const url = environment.contarFacebookUrl;
    this.http.post(url, {id: dipu.key, tipoLegis:dipu.dbpath, campaign:'aborto'}, {responseType: 'text'}).subscribe(res => console.log(res));
    //window.open(`https://m.me/${dipu.facebook}`, "_blank");
    window.open(`https://facebook.com/${dipu.facebook}`, "_blank");
  }

}
