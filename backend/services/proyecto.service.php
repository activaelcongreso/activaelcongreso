<?php

require('model/proyecto.php');

class ProyectoService extends BaseService {

    public static function TraerTodos() {
        $conn = parent::doConnection();
        $result = $conn->query("SELECT * FROM u163305719_aec.Proyecto");
        $listaProy = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $newProyecto = new Proyecto($row['Id'], $row['Nombre'], $row['FotoExt'], 
                $row['Descripcion'], $row['Objetivo'], $row['VerCont'], $row['VerVid'], 
                $row['VerArtic'], $row['Img'], $row['Articulo'], $row['Contador']);
                $newProyecto->twits = ProyTwitService::TraerPorProyecto($row['Id']);
                $newProyecto->videos = ProyVidService::TraerPorProyecto($row['Id']);
                array_push($listaProy, $newProyecto);
            }
        }
        $conn->close();
        return $listaProy;
    }    

    public static function Update($proyecto) {
        $conn = parent::doConnection();
        $sql = "UPDATE u163305719_aec.Proyecto
                SET Nombre = '$proyecto->nombre', FotoExt = '$proyecto->fotoExt', 
                Descripcion = '$proyecto->descripcion', Objetivo = '$proyecto->objetivo', 
                VerCont = '$proyecto->verCont', VerVid = '$proyecto->verVid', VerArtic = '$proyecto->verArtic', 
                Img = '$proyecto->img', Articulo = '$proyecto->articulo', Contador = $proyecto->contador
                WHERE Id = '$proyecto->id'";
        $conn->query($sql);
        $conn->close();
        ProyTwitService::Update($proyecto->twits, $proyecto->id);
        ProyVidService::Update($proyecto->videos, $proyecto->id);
        return $sql;
    }

    public static function Create($proyecto) {
        $conn = parent::doConnection();
        $sql = "INSERT INTO u163305719_aec.Proyecto (Id, Nombre, FotoExt, Descripcion, Objetivo, VerCont, VerVid,
                            VerArtic, Img, Articulo, Contador) 
                VALUES ('$proyecto->id', '$proyecto->nombre', '$proyecto->fotoExt', '$proyecto->descripcion', 
                        '$proyecto->objetivo', '$proyecto->verCont', '$proyecto->verVid', 
                        '$proyecto->verArtic', '$proyecto->img', '$proyecto->articulo', $proyecto->contador)";
        $conn->query($sql);
        ProyTwitService::Update($proyecto->twits, $proyecto->id);
        ProyVidService::Update($proyecto->videos, $proyecto->id);
        PosLegService::CreateByProy($proyecto->id);
        $conn->close();
    }

    public static function Delete($proyecto) {
        $conn = parent::doConnection();
        ProyTwitService::DeleteByProy($proyecto->id);
        PosLegService::DeleteByProy($proyecto->id);
        $sql = "DELETE FROM u163305719_aec.Proyecto WHERE Id='$proyecto->id'";
        $conn->query($sql);
        $conn->close();
    }

    public static function AumentarContador($id, $newNumber) {
        $conn = parent::doConnection();
        $sql = "UPDATE u163305719_aec.Proyecto SET Contador = $newNumber WHERE Id = '$id'";
        $conn->query($sql);
    }

}

?>