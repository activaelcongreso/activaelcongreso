<?php

class Proyecto {

    public $id;
    public $nombre;
    public $fotoExt;
    public $descripcion;
    public $objetivo;
    public $verCont;
    public $verVid;
    public $verArtic;
    public $img;
    public $articulo;
    public $contador;
    //Otros objetos
    public $twits;
    public $videos;

    function __construct($id, $nombre, $fotoExt, $descripcion, $objetivo, 
            $verCont, $verVid, $verArtic, $img, $articulo, $contador) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->fotoExt = $fotoExt;
        $this->descripcion = $descripcion;
        $this->objetivo = $objetivo;
        $this->verCont = $verCont;
        $this->verVid = $verVid;
        $this->verArtic = $verArtic;
        $this->img = $img;
        $this->articulo = $articulo;
        $this->contador = $contador;
    }


}

?>