<?php

class PosLeg {
    
    public $id;
    public $legisladorx;
    public $proyecto;
    public $posicion;

    function __construct($id, $legisladorx, $proyecto, $posicion) {
        $this->id = $id;
        $this->legisladorx = $legisladorx;
        $this->proyecto = $proyecto;
        $this->posicion = $posicion;
    }

}

?>