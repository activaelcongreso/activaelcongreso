<?php

class Diputadx {

    public $id;
    public $nombre;
    public $apellido;
    public $bloque;
    public $distrito;
    public $espacio;
    public $imgExt;
    public $estadoCivil;
    public $twitter;
    public $interno;
    public $fechaNac;
    public $img;
    public $cargo;
    public $facebook;
    public $numeroCompleto;
    //Otros objetos
    public $posiciones;

    function __construct($id, $nombre, $apellido, $bloque, $espacio, $distrito, $imgExt, 
                        $estadoCivil, $twitter, $interno, $fechaNac, $img, $cargo, 
                        $facebook, $numeroCompleto) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->bloque = $bloque;
        $this->distrito = $distrito;
        $this->espacio = $espacio;
        $this->imgExt = $imgExt;
        $this->estadoCivil = $estadoCivil;
        $this->twitter = $twitter;
        $this->interno = $interno;
        $this->fechaNac = $fechaNac;
        $this->img = $img;
        $this->cargo = $cargo;
        $this->facebook = $facebook;
        $this->numeroCompleto = $numeroCompleto;
    }


}

?>