<?php

require('model/diputadx.php');

class DiputadxService extends BaseService {

    public static function TraerTodos() {
        $conn = parent::doConnection();
        $result = $conn->query("SELECT * FROM u163305719_aec.Diputadx");
        $listaDip = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $newDip = new Diputadx($row['Id'], $row['Nombre'], $row['Apellido'] ,$row['Bloque']
                    ,$row['Espacio'], $row['Distrito'],$row['ImgExt'], $row['EstadoCivil'] ,$row['Twitter']
                    ,$row['Interno'], $row['FechaNac'], $row['Img'], $row['Cargo'], $row['Facebook']
                    ,$row['NumeroCompleto']);
                $newDip->posiciones = PosLegService::TraerPorLegisladorx($row['Id']);
                array_push($listaDip, $newDip);
            }
        }
        $conn->close();
        return $listaDip;
    }    

    public static function Update($diputadx) {
        $conn = parent::doConnection();
        $sql = "UPDATE u163305719_aec.Diputadx
                SET Nombre = '$diputadx->nombre', Apellido = '$diputadx->apellido', Bloque = '$diputadx->bloque', 
                Espacio = '$diputadx->espacio', Distrito = '$diputadx->distrito', ImgExt = '$diputadx->imgExt',
                EstadoCivil = '$diputadx->estadoCivil', Twitter = '$diputadx->twitter', 
                Interno = '$diputadx->interno', FechaNac = '$diputadx->fechaNac', Img = '$diputadx->img',
                Cargo = '$diputadx->cargo', Facebook = '$diputadx->facebook', 
                NumeroCompleto = '$diputadx->numeroCompleto'
                WHERE Id = $diputadx->id";
        $conn->query($sql);
        PosLegService::Update($diputadx->posiciones, $diputadx->id);
        $conn->close();
    }

    public static function Create($diputadx) {
        $conn = parent::doConnection();
        $sql = "INSERT INTO u163305719_aec.Diputadx (Nombre, Apellido, Bloque, Espacio, Distrito, ImgExt,
                            EstadoCivil, Twitter, Interno, FechaNac, Img, Cargo, Facebook,
                            NumeroCompleto) 
                VALUES ('$diputadx->nombre', '$diputadx->apellido', '$diputadx->bloque', 
                        '$diputadx->espacio', '$diputadx->distrito', '$diputadx->imgExt', 
                        '$diputadx->estadoCivil', '$diputadx->twitter', '$diputadx->interno',
                        '$diputadx->fechaNac', '$diputadx->img', '$diputadx->cargo', 
                        '$diputadx->facebook', '$diputadx->numeroCompleto')";
        $conn->query($sql);
        $result = $conn->query("SELECT * FROM u163305719_aec.Diputadx ORDER BY Id DESC LIMIT 1");
        PosLegService::CreateByLeg($result->fetch_assoc()['Id']);
        $conn->close();
    } 

    public static function Delete($diputadx) {
        PosLegService::DeleteByLagis($diputadx->id);
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.Diputadx WHERE Id='$diputadx->id'";
        $conn->query($sql);
        $conn->close();
    } 

}

?>