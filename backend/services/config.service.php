<?php

class ConfigService extends BaseService {
    
    public static function GetContador() {
        $conn = parent::doConnection();
        $result = $conn->query("SELECT * FROM u163305719_aec.Configuracion LIMIT 1");
        $number = $result->fetch_assoc()['Contador'];
        return $number;
    }

    public static function AumentarContador($newNumber) {
        $conn = parent::doConnection();
        $sql = "UPDATE u163305719_aec.Configuracion SET Contador = $newNumber WHERE Id = 1";
        $conn->query($sql);
        $conn->close();
    }


}

?>