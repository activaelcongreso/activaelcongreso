<?php

require('model/proyVid.php');

class ProyVidService extends BaseService {

    public static function TraerPorProyecto($proyecto) {
        $conn = parent::doConnection();
        $sql = "SELECT * FROM u163305719_aec.ProyVid
                WHERE Proyecto = '$proyecto'";
        $result = $conn->query($sql);
        $listaVid = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($listaVid, new ProyVid($row['Id'], $row['Proyecto'], $row['Video']));
            }
        }
        $conn->close();
        return $listaVid;
    }

    public static function Update($videos, $proyecto) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.ProyVid WHERE Proyecto = '$proyecto'";
        $conn->query($sql);
        foreach($videos as $vi) { 
            $sql = "INSERT INTO u163305719_aec.ProyVid (Proyecto, Video) VALUES ('$proyecto', '$vi->video')";
            $conn->query($sql);
        }
        $conn->close();
        return $sql;
    }

    public static function DeleteByProy($proyecto) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.ProyVid WHERE Proyecto = '$proyecto'";
        $conn->query($sql);
        $conn->close();
    }

}

?>