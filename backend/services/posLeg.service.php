<?php

require('model/posLeg.php');

class PosLegService extends BaseService {

    public static function TraerPorLegisladorx($legisladorx) {
        $conn = parent::doConnection();
        $sql = "SELECT * FROM u163305719_aec.PosLeg
                WHERE Legisladorx = '$legisladorx'";
        $result = $conn->query($sql);
        $listaPos = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($listaPos, new PosLeg($row['Id'], $row['Legisladorx'], 
                    $row['Proyecto'], $row['Posicion']));
            }
        }
        $conn->close();
        return $listaPos;
    }

    public static function Update($posiciones, $legisladorx) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.PosLeg WHERE Legisladorx = '$legisladorx'";
        $conn->query($sql);
        foreach($posiciones as $pos) { 
            $sql = "INSERT INTO u163305719_aec.PosLeg (Legisladorx, Proyecto, Posicion) 
                    VALUES ('$legisladorx', '$pos->proyecto', '$pos->posicion')";
            $conn->query($sql);
        }
        $conn->close();
        return $sql;
    }

    public static function CreateByLeg($legisladorx) {
        $listaProy = ProyectoService::TraerTodos();
        $conn = parent::doConnection();
        foreach($listaProy as $proy) {
            $sql = "INSERT INTO u163305719_aec.PosLeg (Legisladorx, Proyecto, Posicion)
                    VALUES ('$legisladorx', '$proy->id', 'NoConfirmado')";
            $conn->query($sql);
        }
    }

    public static function CreateByProy($proyecto) {
        $listaDip = DiputadxService::TraerTodos();
        $conn = parent::doConnection();
        foreach($listaDip as $dip) {
            $sql = "INSERT INTO u163305719_aec.PosLeg (Legisladorx, Proyecto, Posicion)
                    VALUES ('$dip->id', '$proyecto', 'NoConfirmado')";
            $conn->query($sql);
        }
    }

    public static function DeleteByLagis($legisladorx) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.PosLeg WHERE Legisladorx = '$legisladorx'";
        $conn->query($sql);
        $conn->close();
    }

    public static function DeleteByProy($proyecto) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.PosLeg WHERE Proyecto = '$proyecto'";
        $conn->query($sql);
        $conn->close();
    }

}

?>