<?php

require('model/proyTwit.php');

class ProyTwitService extends BaseService {

    public static function TraerPorProyecto($proyecto) {
        $conn = parent::doConnection();
        $sql = "SELECT * FROM u163305719_aec.ProyTwit
                WHERE Proyecto = '$proyecto'";
        $result = $conn->query($sql);
        $listaTwit = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($listaTwit, new ProyTwit($row['Id'], $row['Proyecto'], $row['Twit'], $row['Posicion']));
            }
        }
        $conn->close();
        return $listaTwit;
    }

    public static function Update($twits, $proyecto) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.ProyTwit WHERE Proyecto = '$proyecto'";
        $conn->query($sql);
        foreach($twits as $tw) { 
            $sql = "INSERT INTO u163305719_aec.ProyTwit (Proyecto, Twit, Posicion) VALUES ('$proyecto', '$tw->twit', '$tw->posicion')";
            $conn->query($sql);
        }
        $conn->close();
        return $sql;
    }

    public static function DeleteByProy($proyecto) {
        $conn = parent::doConnection();
        $sql = "DELETE FROM u163305719_aec.ProyTwit WHERE Proyecto = '$proyecto'";
        $conn->query($sql);
        $conn->close();
    }

}

?>