<?php

    require 'services/vendor/autoload.php';
    require 'services/base.service.php';
    require 'services/diputadx.service.php';
    require 'services/proyecto.service.php';
    require 'services/proyTwit.service.php';
    require 'services/proyVid.service.php';
    require 'services/posLeg.service.php';
    require 'services/config.service.php';

    $app = new Slim\App();
    
    $app->options('/{routes:.+}', function ($request, $response, $args) {
        return $response;
    });
    
    $app->add(function ($req, $res, $next) {
        $response = $next($req, $res);
        return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    });

    /* Politicxs */
    $app->get('/traerDip', function($request, $response, $args) {
        $dipArray = DiputadxService::TraerTodos();
        return json_encode($dipArray);
    });

    $app->post('/updateDip', function($request, $response, $args) {
        $litaDip = json_decode($request->getBody()); 
        foreach($litaDip as $dip) { 
            DiputadxService::Update($dip);
        }
        return json_encode($litaDip);
    });

    $app->post('/createDip', function($request, $response, $args) {
        $dip = json_decode($request->getBody()); 
        return DiputadxService::Create($dip);
    });

    $app->post('/deleteDip', function($request, $response, $args) {
        $dip = json_decode($request->getBody()); 
        DiputadxService::Delete($dip);
    });

    /* Proyectos */
    $app->get('/traerProy', function($request, $response, $args) { 
        $proyArray = ProyectoService::TraerTodos();
        return json_encode($proyArray);
    });

    $app->post('/updateProy', function($request, $response, $args) {
        $litaProy = json_decode($request->getBody()); 
        foreach($litaProy as $proy) { 
            ProyectoService::Update($proy); 
        }
    });

    $app->post('/createProy', function($request, $response, $args) {
        $proy = json_decode($request->getBody()); 
        ProyectoService::Create($proy);
        
    });

    $app->post('/deleteProy', function($request, $response, $args) {
        $proy = json_decode($request->getBody()); 
        ProyectoService::Delete($proy);
    });

    $app->post('/aumentarContProy', function($request, $response, $args) {
        $data = json_decode($request->getBody());
        return ProyectoService::AumentarContador($data->id, $data->newNumber);
    });

    /* Contador */
    $app->get('/getCont', function($request, $response, $args) {
        return ConfigService::GetContador();
    });
    
    $app->post('/aumentarCont', function($request, $response, $args) {
        $newNumber = json_encode(json_decode($request->getBody()));
        ConfigService::AumentarContador($newNumber);
        return ConfigService::GetContador();
    });

    $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
        $handler = $this->notFoundHandler; 
        return $handler($req, $res);
    });

    $app->run();

?>